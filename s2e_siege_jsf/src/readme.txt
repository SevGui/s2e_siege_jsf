Nom du projet : s2e_siege_jsf
Techno : jsf/Hibernate
Pour qu'il fonctionne chez vous, il faut mettre vos données personnelles dans :
- la classe com.greta.s2e_siege_jsf.globals.Constantes
- le fichier de configuration de hibernate : src/hibernate.cfg.xml

! Ne pas oublier de mettre le driver JDBC : mysql-connector-java-5.1.40-bin.jar dans 
le dossier lib de Tomcat (C:\Program Files\Apache Software Foundation\Tomcat 8.0\lib)

! Bibliothèque à télécharger pour utiliser les constantes dans xhtml
http://omnifaces.org/ cliquer sur  OmniFaces 2.5.1

Bibliothèques à mettre dans /WEB-INF/lib/ :
antlr-2.7.7.jar
aopalliance-1.0.jar
asm-5.0.4.jar
asm-attrs-2.2.3.jar
c3p0-0.9.2.1.jar
cdi-api-2.0-EDR2.jar
cglib-3.1.jar
commons-beanutils-1.9.2.jar
commons-collections-3.2.1.jar
commons-collections-3.2.2.jar
commons-digester-1.8.jar
commons-logging-1.1.1.jar
commons-logging-1.2.jar
dom4j-1.6.1.jar
ehcache-2.10.1.jar
geronimo-atinject_1.0_spec-1.0.jar
geronimo-jta_1.1_spec-1.1.1.jar
hibernate-commons-annotations-5.0.0.Final.jar
hibernate-core-5.0.2.Final.jar
hibernate-entitymanager-5.0.2.Final.jar
hibernate-jpa-2.1-api-1.0.0.Final.jar
hibernate-validator-5.2.2.Final.jar
jandex-1.2.2.Final.jar
javassist-3.18.1-GA.jar
javax.inject-1.0-PFD-1.jar
joda-time-2.9.4.jar
jstl-1.2.jar
jta-1.1.jar
log4j-1.2.17.jar
myfaces-api-2.2.11.jar
myfaces-bundle-2.2.11.jar
myfaces-impl-2.2.11.jar
ojdbc7.jar
omnifaces-2.5.1.jar
persistence-api-1.0.2.jar
weld-servlet-2.4.0.Final.jar