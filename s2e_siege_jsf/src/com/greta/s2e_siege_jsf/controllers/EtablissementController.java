package com.greta.s2e_siege_jsf.controllers;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.greta.s2e_siege_jsf.beans.EtablissementBean;
import com.greta.s2e_siege_jsf.services.EtablissementServiceImpl;

@Named
@SessionScoped
public class EtablissementController implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(EtablissementController.class);
	private List<EtablissementBean> etabList;
	private EtablissementServiceImpl etabService;
	private int etabId;
	private String etabNom;
	
	/**
	 * Constructeur
	 */
	public EtablissementController() {
		LOGGER.info("Controller constructeur");
		etabService = new EtablissementServiceImpl();
		fillEtabList();
	}
	
	/*
	 * Récupération de la liste des établissements
	 */
	private void fillEtabList() {
		LOGGER.info("Controller fillEtabList");
		etabList = new ArrayList<>();
		etabList = etabService.listEtab();
	}
	
	/*
	 * Récupération de la liste des établissements
	 * Retour vers la page listbudget
	 */
	public String listPage() {
		LOGGER.info("Controller listPage");
		fillEtabList();
		return BUDGET_PAGE;
	}
	
	/*
	 * Récupérationdes des informations d'un établissement
	 * par son id
	 */
	public String updateBudget(int id) {
		LOGGER.info("Controller updateBudget id = " + id);
		EtablissementBean etab = new EtablissementBean();
		etab = etabService.getElementById(id);
		setEtabId(etab.getEtabId());
		setEtabNom(etab.getEtabNom());
		setEtabBudget(etab.getEtabBudget());
		LOGGER.info("Controller updateBudget name = " + getEtabNom());
		return UPDATE_PAGE;
	}
	
	/*
	 * Modification du budget en fonction d'un établissement
	 */
	public String updatingBudget() {
		LOGGER.info("Controller updatingBudget");
		LOGGER.info("Controller updatingBudget budget, name = " + etabBudget + " " + etabNom);
		EtablissementBean etab = new EtablissementBean(etabId, etabNom, etabBudget);
		etabService.update(etab);
		fillEtabList();
		return BUDGET_PAGE;
	}

	/**
	 * @return the etabId
	 */
	public int getEtabId() {
		return etabId;
	}

	/**
	 * @param etabId the etabId to set
	 */
	public void setEtabId(int etabId) {
		this.etabId = etabId;
	}

	/**
	 * @return the etabNom
	 */
	public String getEtabNom() {
		return etabNom;
	}

	/**
	 * @param etabNom the etabNom to set
	 */
	public void setEtabNom(String etabNom) {
		this.etabNom = etabNom;
	}

	/**
	 * @return the etabBudget
	 */
	public float getEtabBudget() {
		return etabBudget;
	}

	/**
	 * @param etabBudget the etabBudget to set
	 */
	public void setEtabBudget(float etabBudget) {
		this.etabBudget = etabBudget;
	}

	private float etabBudget;

	
	/**
	 * @return the etabList
	 */
	public List<EtablissementBean> getEtabList() {
		return etabList;
	}

	/**
	 * @param etabList the etabList to set
	 */
	public void setEtabList(List<EtablissementBean> etabList) {
		this.etabList = etabList;
	}

}
