package com.greta.s2e_siege_jsf.controllers;


import static com.greta.s2e_siege_jsf.globals.Constants.*;

import java.io.IOException;
import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.greta.s2e_siege_jsf.utils.SessionUtils;

@Named
@SessionScoped
public class DeconnexionController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(DeconnexionController.class);

	public void deconnexion() throws IOException {
		LOGGER.info("DeconnexionController");
		//récupération de la session
		HttpSession session = SessionUtils.getSession();
		//récupération de la réponse
		HttpServletResponse response = SessionUtils.getResponse();
		//supression de la session en cours
		session.invalidate();
		//retour à la page de connexion
		response.sendRedirect(REDIRECTION_LOGIN);
	}
}
