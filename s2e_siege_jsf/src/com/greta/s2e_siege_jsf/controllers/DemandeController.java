package com.greta.s2e_siege_jsf.controllers;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;

import org.apache.log4j.Logger;

import com.greta.s2e_siege_jsf.beans.DemandeBean;
import com.greta.s2e_siege_jsf.beans.EtablissementBean;
import com.greta.s2e_siege_jsf.beans.EtatBean;
import com.greta.s2e_siege_jsf.services.DemandeServiceImpl;

@Named
@SessionScoped
public class DemandeController implements Serializable {

	private static final long serialVersionUID = 1L;
	private static final Logger LOGGER = Logger.getLogger(DemandeController.class);
	private DemandeServiceImpl dmdService;
	private List<DemandeBean> dmdList;
	private DemandeBean dmd;

	/**
	 * Constructeur
	 */
	public DemandeController() {
		LOGGER.info("Controller constructeur");
		dmdService = new DemandeServiceImpl();
		fillDmdList();
	}

	/*
	 * Récupération de la liste des demandes
	 */
	private void fillDmdList() {
		LOGGER.info("Controller fillDmdList");
		dmdList = new ArrayList<>();
		dmdList = dmdService.listDemande();
	}
	
	/*
	 * Récupération de la liste des demandes
	 * Retour vers la page listdemande
	 */
	public String listDmd() {
		LOGGER.info("Controller listDmd");
		fillDmdList();
		return DEMANDE_PAGE;
	}

	/*
	 * Modification de l'état de la demande lorsque
	 * l'état est à valider
	 */
	public String validateDmd(int dmdId) {
		LOGGER.info("Controller validateDmd id = " + dmdId);
		dmd = new DemandeBean(dmdId, new EtatBean(VALI_ETAT));
		dmdService.update(dmd);
		fillDmdList();
		return DEMANDE_PAGE;
	}

	/*
	 * Modification de l'état de la demande lorsque
	 * l'état est à refuser
	 */
	public String refuseDmd(int dmdId) {
		LOGGER.info("Controller refuseDmd id = " + dmdId);
		dmd = new DemandeBean(dmdId, new EtatBean(REFU_ETAT));
		dmdService.update(dmd);
		fillDmdList();
		return DEMANDE_PAGE;
	}

	/**
	 * @return the dmdList
	 */
	public List<DemandeBean> getDmdList() {
		return dmdList;
	}

	/**
	 * @param dmdList
	 *            the dmdList to set
	 */
	public void setDmdList(List<DemandeBean> dmdList) {
		this.dmdList = dmdList;
	}
}
