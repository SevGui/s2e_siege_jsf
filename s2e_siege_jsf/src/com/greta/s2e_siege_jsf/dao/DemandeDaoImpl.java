package com.greta.s2e_siege_jsf.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import com.greta.s2e_siege_jsf.beans.DemandeBean;

public class DemandeDaoImpl implements DemandeDao{
	
	private static final Logger LOGGER = Logger.getLogger(DemandeDaoImpl.class);

	/*
	 * Récupération d'une liste de demandes
	 */
	@Override
	public List<DemandeBean> listDmd() {
		LOGGER.info("Dao listDmd");
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		String hql = "FROM DemandeBean";
		Query query = session.createQuery(hql);
		List<DemandeBean> dmdList = query.list();
		if (LOGGER.isInfoEnabled()){
			for (DemandeBean dmd : dmdList) {
				LOGGER.info(dmd.toString());
			}
		}
		session.close();
		return dmdList;
	}

	@Override
	public int create(DemandeBean dmd) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * Modification d'une demande
	 */
	@Override
	public int update(DemandeBean dmd) {
		LOGGER.info("Dao update dmd");
		int result = 0;
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		String hql = "UPDATE DemandeBean set dmdIdEtat = :dmdIdEtat WHERE dmdId = :dmdId";
		Query query = session.createQuery(hql);
		query.setParameter("dmdIdEtat", dmd.getEtatBean().getEtatId());
		query.setParameter("dmdId", dmd.getDmdId());
		result = query.executeUpdate();
		session.close();
		return result;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public DemandeBean getElementById(int id) {
		// TODO Auto-generated method stub
		return null;
	}

}
