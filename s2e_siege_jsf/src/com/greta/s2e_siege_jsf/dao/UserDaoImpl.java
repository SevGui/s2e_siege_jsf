package com.greta.s2e_siege_jsf.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.cfg.Configuration;

import com.greta.s2e_siege_jsf.beans.EtablissementBean;
import com.greta.s2e_siege_jsf.beans.UserBean;

public class UserDaoImpl implements UserDao{
	
	private static final Logger LOGGER = Logger.getLogger(UserDaoImpl.class);

	@Override
	public List<UserBean> listUser() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public int create(UserBean user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(UserBean user) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * Récupération d'un utilisateur en fonction de son ID
	 */
	@Override
	public UserBean getElementById(int id) {
		LOGGER.info("Dao ElementById");
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		LOGGER.info("test");
		UserBean user = session.load(UserBean.class, id);
		LOGGER.info("loaded user");
		return user;
	}

}
