package com.greta.s2e_siege_jsf.dao;

import java.util.List;

import com.greta.s2e_siege_jsf.beans.EtablissementBean;

public interface EtablissementDao {

	public List<EtablissementBean> listEtab();
	public int create(EtablissementBean etab);
	public int update(EtablissementBean etab);
	public int delete(int id);
	public EtablissementBean getElementById(int id);
	
}
