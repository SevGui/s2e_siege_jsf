package com.greta.s2e_siege_jsf.dao;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;

import com.greta.s2e_siege_jsf.beans.EtablissementBean;

public class EtablissementDaoImpl implements EtablissementDao{
	
	private static final Logger LOGGER = Logger.getLogger(EtablissementDaoImpl.class);

	/*
	 * Récupération d'une liste d'établissement
	 */
	@Override
	public List<EtablissementBean> listEtab() {
		LOGGER.info("Dao listEtab");
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		String hql = "FROM EtablissementBean";
		Query query = session.createQuery(hql);
		List<EtablissementBean> userList = query.list();
		session.close();
		return userList;
	}

	@Override
	public int create(EtablissementBean etab) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * modification d'un établissement
	 */
	@Override
	public int update(EtablissementBean etab) {
		LOGGER.info("Dao update etab");
		int result = 0;
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		String hql = "UPDATE EtablissementBean set etabBudget = :etabBudget WHERE etabId = :etabId";
		Query query = session.createQuery(hql);
		query.setParameter("etabBudget", etab.getEtabBudget());
		query.setParameter("etabId", etab.getEtabId());
		result = query.executeUpdate();
		session.close();
		return result;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	/*
	 * Récupération d'un établissement en fonction de son ID
	 */
	@Override
	public EtablissementBean getElementById(int id) {
		LOGGER.info("Dao ElementById");
		Session session = new Configuration().configure().buildSessionFactory().openSession();
		LOGGER.info("test");
		EtablissementBean etab = session.load(EtablissementBean.class, id);
		LOGGER.info("loaded user");
		return etab;
	}

}
