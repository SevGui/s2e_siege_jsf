package com.greta.s2e_siege_jsf.dao;

import java.util.List;

import com.greta.s2e_siege_jsf.beans.DemandeBean;

public interface DemandeDao {

	public List<DemandeBean> listDmd();
	public int create(DemandeBean dmd);
	public int update(DemandeBean dmd);
	public int delete(int id);
	public DemandeBean getElementById(int id);
}
