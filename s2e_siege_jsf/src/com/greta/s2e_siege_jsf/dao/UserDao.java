package com.greta.s2e_siege_jsf.dao;

import java.util.List;

import com.greta.s2e_siege_jsf.beans.UserBean;

public interface UserDao {

	public List<UserBean> listUser();
	public int create(UserBean user);
	public int update(UserBean user);
	public int delete(int id);
	public UserBean getElementById(int id);
}
