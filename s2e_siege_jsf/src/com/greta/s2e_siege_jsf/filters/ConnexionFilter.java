package com.greta.s2e_siege_jsf.filters;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import com.greta.s2e_siege_jsf.beans.UserBean;
import com.greta.s2e_siege_jsf.dao.UserDao;
import com.greta.s2e_siege_jsf.dao.UserDaoImpl;


public class ConnexionFilter implements Filter {

	private static final Logger LOGGER = Logger.getLogger(ConnexionFilter.class);

	@Override
	public void destroy() {
		// TODO Auto-generated method stub

	}

	@Override
	public void doFilter(ServletRequest oneRequest, ServletResponse oneResponse, FilterChain chain)
			throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) oneRequest;
		HttpServletResponse response = (HttpServletResponse) oneResponse;
		HttpSession session = request.getSession();
		Cookie[] cookies = request.getCookies();

		UserBean utilisateur = null;

		// récupération de l'utilisateur à partir du cookie générique créé dans
		// le site de connexion
		if (null != cookies) {
			for (Cookie cookie : cookies) {
				if (cookie.getName().equals(COOKIE_S2E)) {
					LOGGER.info("Récupération du cookie générique : " + cookie.getValue());
					utilisateur = getUtilisateurById(cookie.getValue());
					break;
				}
			}
			// destruction du cookie générique
			destructionCookie(response);
		}
		
		//si l'utilisateur est null, récupération de l'utilisateur dans la session
		if (null == utilisateur){
			utilisateur = (UserBean) session.getAttribute(SESSION_S2E);
		}
		
		//si l'utilisateur n'est pas retrouvé ou s'il n'a pas le droit d'être là 
		if(null == utilisateur || !utilisateur.getUtlIdRole().equals(DROIT_ETABLISSEMENT)){
			LOGGER.info("Utilisateur non reconnu ou non autorisé : redirection sur la page de connexion");
			//retour à la page de connexion
			response.sendRedirect(REDIRECTION_LOGIN);
		} else {	//tout est ok
			LOGGER.info("Utilisateur connecté");
			//enregistrement de l'utilisateur en session
	        session.setAttribute( SESSION_S2E, utilisateur );
	        //poursuite de la navigation
			chain.doFilter(request, response);
		}

	}

	private UserBean getUtilisateurById(String id) {
		if (id.equals(null) || id.isEmpty() || id.equals(""))
			return null;
		UserDao userDao = new UserDaoImpl();
		return userDao.getElementById(Integer.valueOf(id));
	}

	private void destructionCookie(HttpServletResponse response) {
		Cookie cookie = new Cookie(COOKIE_S2E, "");
		cookie.setPath(COOKIE_S2E_PATH);
		cookie.setMaxAge(-1);
		response.addCookie(cookie);

		LOGGER.info("Destruction du cookie");

	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub

	}

}
