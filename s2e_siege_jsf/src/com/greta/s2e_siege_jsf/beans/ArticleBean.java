package com.greta.s2e_siege_jsf.beans;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Named;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name=ARTICLES)
public class ArticleBean {

	@Id
	@Column (name = ART_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int artId;
	@Column (name = ART_NOM)
	private String artNom;
	@Column (name = ART_MARQUE)
	private String artMarque;
	@Column (name = ART_MODELE)
	private String artModele;
	@Column (name = ART_EQUI)
	private int artEqui;
	@Column (name = ART_PRIXM)
	private Float artPrixMoyen;
	@Column (name = ART_QTETA)
	private int artQuanTA;
	
	@OneToMany(mappedBy = "articleBean")
	List<DemandeBean> demande = new ArrayList<>();
	@OneToOne (mappedBy = "articleBean")
	private StockBean stockBean;
	
	/**
	 * @return the artId
	 */
	public int getArtId() {
		return artId;
	}
	/**
	 * @param artId the artId to set
	 */
	public void setArtId(int artId) {
		this.artId = artId;
	}
	/**
	 * @return the artNom
	 */
	public String getArtNom() {
		return artNom;
	}
	/**
	 * @param artNom the artNom to set
	 */
	public void setArtNom(String artNom) {
		this.artNom = artNom;
	}
	/**
	 * @return the artMarque
	 */
	public String getArtMarque() {
		return artMarque;
	}
	/**
	 * @param artMarque the artMarque to set
	 */
	public void setArtMarque(String artMarque) {
		this.artMarque = artMarque;
	}
	/**
	 * @return the artModele
	 */
	public String getArtModele() {
		return artModele;
	}
	/**
	 * @param artModele the artModele to set
	 */
	public void setArtModele(String artModele) {
		this.artModele = artModele;
	}
	/**
	 * @return the artEqui
	 */
	public int getArtEqui() {
		return artEqui;
	}
	/**
	 * @param artEqui the artEqui to set
	 */
	public void setArtEqui(int artEqui) {
		this.artEqui = artEqui;
	}
	/**
	 * @return the artPrixMoyen
	 */
	public Float getArtPrixMoyen() {
		return artPrixMoyen;
	}
	/**
	 * @param artPrixMoyen the artPrixMoyen to set
	 */
	public void setArtPrixMoyen(Float artPrixMoyen) {
		this.artPrixMoyen = artPrixMoyen;
	}
	/**
	 * @return the artQuanTA
	 */
	public int getArtQuanTA() {
		return artQuanTA;
	}
	/**
	 * @param artQuanTA the artQuanTA to set
	 */
	public void setArtQuanTA(int artQuanTA) {
		this.artQuanTA = artQuanTA;
	}
	/**
	 * @return the demande
	 */
	public List<DemandeBean> getDemande() {
		return demande;
	}
	/**
	 * @param demande the demande to set
	 */
	public void setDemande(List<DemandeBean> demande) {
		this.demande = demande;
	}
	/**
	 * @return the stockBean
	 */

	public StockBean getStockBean() {
		return stockBean;
	}
	/**
	 * @param stockBean the stockBean to set
	 */
	public void setStockBean(StockBean stockBean) {
		this.stockBean = stockBean;
	}

	
	
}
