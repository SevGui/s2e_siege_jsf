package com.greta.s2e_siege_jsf.beans;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = DEMANDE)
public class DemandeBean {
	
	@Id
	@Column (name = DMD_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int dmdId;
	@Column (name = DMD_QTE)
	private int dmdQuant;
	@Column (name = DMD_DELAI)
	private int dmddelai;
	@Column (name = DMD_DATE)
	private Date dmddate;
	@ManyToOne
	@JoinColumn (name = DMD_ID_USER)
	private UserBean userBean;
	@ManyToOne
	@JoinColumn (name = DMD_ID_ART)
	private ArticleBean articleBean;
	@ManyToOne
	@JoinColumn (name = DMD_ID_ETAT)
	private EtatBean etatBean;
	
	
	
	/**
	 * 
	 */
	public DemandeBean() {
		super();
	}
	/**
	 * @param dmdId
	 * @param etatBean
	 */
	public DemandeBean(int dmdId, EtatBean etatBean) {
		super();
		this.dmdId = dmdId;
		this.etatBean = etatBean;
	}
	/**
	 * @return the dmdId
	 */
	public int getDmdId() {
		return dmdId;
	}
	/**
	 * @param dmdId the dmdId to set
	 */
	public void setDmdId(int dmdId) {
		this.dmdId = dmdId;
	}

	/**
	 * @return the dmdQuant
	 */
	public int getDmdQuant() {
		return dmdQuant;
	}
	/**
	 * @param dmdQuant the dmdQuant to set
	 */
	public void setDmdQuant(int dmdQuant) {
		this.dmdQuant = dmdQuant;
	}
	/**
	 * @return the dmddelai
	 */
	public int getDmddelai() {
		return dmddelai;
	}
	/**
	 * @param dmddelai the dmddelai to set
	 */
	public void setDmddelai(int dmddelai) {
		this.dmddelai = dmddelai;
	}


	/**
	 * @return the dmddate
	 */
	public Date getDmddate() {
		return dmddate;
	}
	/**
	 * @param dmddate the dmddate to set
	 */
	public void setDmddate(Date dmddate) {
		this.dmddate = dmddate;
	}
	/**
	 * @return the userBean
	 */
	public UserBean getUserBean() {
		return userBean;
	}
	/**
	 * @param userBean the userBean to set
	 */
	public void setUserBean(UserBean userBean) {
		this.userBean = userBean;
	}

	/**
	 * @return the articleBean
	 */
	public ArticleBean getArticleBean() {
		return articleBean;
	}
	/**
	 * @param articleBean the articleBean to set
	 */
	public void setArticleBean(ArticleBean articleBean) {
		this.articleBean = articleBean;
	}
	/**
	 * @return the etatBean
	 */
	public EtatBean getEtatBean() {
		return etatBean;
	}
	/**
	 * @param etatBean the etatBean to set
	 */
	public void setEtatBean(EtatBean etatBean) {
		this.etatBean = etatBean;
	}

	
}
