package com.greta.s2e_siege_jsf.beans;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = STOCK)
public class StockBean {

	@Id
	@Column (name = STK_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int stockId;
	@Column (name = STK_QUANT)
	private int stockQuant;
	@Column (name = STK_QUANTMIN)
	private int stockQuantMin;
	@Column (name = STK_QUANTMAX)
	private int stockQuantMax;
	@OneToOne 
	@JoinColumn (name = STK_ID_ART)
	private ArticleBean articleBean;
	
	/**
	 * @return the stockId
	 */
	public int getStockId() {
		return stockId;
	}
	/**
	 * @param stockId the stockId to set
	 */
	public void setStockId(int stockId) {
		this.stockId = stockId;
	}
	/**
	 * @return the stockQuant
	 */
	public int getStockQuant() {
		return stockQuant;
	}
	/**
	 * @param stockQuant the stockQuant to set
	 */
	public void setStockQuant(int stockQuant) {
		this.stockQuant = stockQuant;
	}
	/**
	 * @return the stockQuantMin
	 */
	public int getStockQuantMin() {
		return stockQuantMin;
	}
	/**
	 * @param stockQuantMin the stockQuantMin to set
	 */
	public void setStockQuantMin(int stockQuantMin) {
		this.stockQuantMin = stockQuantMin;
	}
	/**
	 * @return the stockQuantMax
	 */
	public int getStockQuantMax() {
		return stockQuantMax;
	}
	/**
	 * @param stockQuantMax the stockQuantMax to set
	 */
	public void setStockQuantMax(int stockQuantMax) {
		this.stockQuantMax = stockQuantMax;
	}
	/**
	 * @return the articleBean
	 */

	public ArticleBean getArticleBean() {
		return articleBean;
	}
	/**
	 * @param articleBean the articleBean to set
	 */
	public void setArticleBean(ArticleBean articleBean) {
		this.articleBean = articleBean;
	}
	
	
}
