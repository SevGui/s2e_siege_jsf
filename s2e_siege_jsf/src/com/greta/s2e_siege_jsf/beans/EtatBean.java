package com.greta.s2e_siege_jsf.beans;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = ETATS)
public class EtatBean {

	@Id
	@Column (name = ETT_ID)
	private String etatId;
	@Column (name = ETT_LIB)
	private String etatLibelle;
	
	@OneToMany(mappedBy = "etatBean")
	List<DemandeBean> demande = new ArrayList<>();
	
	
	
	/**
	 * 
	 */
	public EtatBean() {
		super();
	}
	/**
	 * @param etatId
	 */
	public EtatBean(String etatId) {
		super();
		this.etatId = etatId;
	}
	/**
	 * @return the etatId
	 */
	public String getEtatId() {
		return etatId;
	}
	/**
	 * @param etatId the etatId to set
	 */
	public void setEtatId(String etatId) {
		this.etatId = etatId;
	}
	/**
	 * @return the etatLibelle
	 */
	public String getEtatLibelle() {
		return etatLibelle;
	}
	/**
	 * @param etatLibelle the etatLibelle to set
	 */
	public void setEtatLibelle(String etatLibelle) {
		this.etatLibelle = etatLibelle;
	}
	/**
	 * @return the demande
	 */
	public List<DemandeBean> getDemande() {
		return demande;
	}
	/**
	 * @param demande the demande to set
	 */
	public void setDemande(List<DemandeBean> demande) {
		this.demande = demande;
	}
	
	
	
}
