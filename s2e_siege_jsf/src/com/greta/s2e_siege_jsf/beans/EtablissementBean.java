package com.greta.s2e_siege_jsf.beans;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = ETAB)
public class EtablissementBean {
	
	@Id
	@Column (name = ETB_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int etabId;
	@Column (name = ETB_NOM)
	private String etabNom;
	@Column (name = ETB_ADR)
	private String etabAdres;
	@Column (name = ETB_CP)
	private int etabCP;
	@Column (name = ETB_VILLE)
	private String etabVille;
	@Column (name = ETB_BUDG)
	private float etabBudget;
	@Column (name = ETB_BUDG_REST)
	private float etabBudRest;
	
	@OneToMany(mappedBy = "etablissementBean")
	List<UserBean> userList = new ArrayList<>();
	
	
	/**
	 * 
	 */
	public EtablissementBean() {
		super();
	}

	/**
	 * @param etabId
	 * @param etabNom
	 * @param etabBudget
	 */
	public EtablissementBean(int etabId, String etabNom, float etabBudget) {
		super();
		this.etabId = etabId;
		this.etabNom = etabNom;
		this.etabBudget = etabBudget;
	}
	
	/**
	 * @return the etabId
	 */
	public int getEtabId() {
		return etabId;
	}
	/**
	 * @param etabId the etabId to set
	 */
	public void setEtabId(int etabId) {
		this.etabId = etabId;
	}
	/**
	 * @return the etabNom
	 */
	public String getEtabNom() {
		return etabNom;
	}
	/**
	 * @param etabNom the etabNom to set
	 */
	public void setEtabNom(String etabNom) {
		this.etabNom = etabNom;
	}
	/**
	 * @return the etabAdres
	 */
	public String getEtabAdres() {
		return etabAdres;
	}
	/**
	 * @param etabAdres the etabAdres to set
	 */
	public void setEtabAdres(String etabAdres) {
		this.etabAdres = etabAdres;
	}
	/**
	 * @return the etabCP
	 */
	public int getEtabCP() {
		return etabCP;
	}
	/**
	 * @param etabCP the etabCP to set
	 */
	public void setEtabCP(int etabCP) {
		this.etabCP = etabCP;
	}
	/**
	 * @return the etabVille
	 */
	public String getEtabVille() {
		return etabVille;
	}
	/**
	 * @param etabVille the etabVille to set
	 */
	public void setEtabVille(String etabVille) {
		this.etabVille = etabVille;
	}
	/**
	 * @return the etabBudget
	 */
	public float getEtabBudget() {
		return etabBudget;
	}
	/**
	 * @param etabBudget the etabBudget to set
	 */
	public void setEtabBudget(float etabBudget) {
		this.etabBudget = etabBudget;
	}

	/**
	 * @return the etabBudRest
	 */
	public float getEtabBudRest() {
		return etabBudRest;
	}

	/**
	 * @param etabBudRest the etabBudRest to set
	 */
	public void setEtabBudRest(float etabBudRest) {
		this.etabBudRest = etabBudRest;
	}

	/**
	 * @return the userList
	 */
	public List<UserBean> getUserList() {
		return userList;
	}

	/**
	 * @param userList the userList to set
	 */
	public void setUserList(List<UserBean> userList) {
		this.userList = userList;
	}

}
