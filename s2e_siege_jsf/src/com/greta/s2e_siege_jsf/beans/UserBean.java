package com.greta.s2e_siege_jsf.beans;

import static com.greta.s2e_siege_jsf.globals.Constants.*;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = USERS)
public class UserBean {

	@Id
	@Column (name = USR_ID)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private int utlId;
	@Column (name = USR_NOM)
	private String utlNom;
	@Column (name = USR_PRENOM)
	private String utlPrenom;
	@Column (name = USR_LOGIN)
	private String utlLogin;
	@Column (name = USR_PASSWORD)
	private String utlMdp;
	@Column (name = USR_ID_ROLE)
	private String utlIdRole;
	@ManyToOne
	@JoinColumn (name = USR_ID_ETB)
	private EtablissementBean etablissementBean;
	
	@OneToMany(mappedBy = "userBean")
	List<DemandeBean> demande = new ArrayList<>();
	
	
	/**
	 * @return the utlId
	 */
	public int getUtlId() {
		return utlId;
	}
	/**
	 * @param utlId the utlId to set
	 */
	public void setUtlId(int utlId) {
		this.utlId = utlId;
	}
	/**
	 * @return the utlNom
	 */
	public String getUtlNom() {
		return utlNom;
	}
	/**
	 * @param utlNom the utlNom to set
	 */
	public void setUtlNom(String utlNom) {
		this.utlNom = utlNom;
	}
	/**
	 * @return the utlPrenom
	 */
	public String getUtlPrenom() {
		return utlPrenom;
	}
	/**
	 * @param utlPrenom the utlPrenom to set
	 */
	public void setUtlPrenom(String utlPrenom) {
		this.utlPrenom = utlPrenom;
	}
	/**
	 * @return the utlLogin
	 */
	public String getUtlLogin() {
		return utlLogin;
	}
	/**
	 * @param utlLogin the utlLogin to set
	 */
	public void setUtlLogin(String utlLogin) {
		this.utlLogin = utlLogin;
	}
	/**
	 * @return the utlMdp
	 */
	public String getUtlMdp() {
		return utlMdp;
	}
	/**
	 * @param utlMdp the utlMdp to set
	 */
	public void setUtlMdp(String utlMdp) {
		this.utlMdp = utlMdp;
	}
	/**
	 * @return the utlIdRole
	 */
	public String getUtlIdRole() {
		return utlIdRole;
	}
	/**
	 * @param utlIdRole the utlIdRole to set
	 */
	public void setUtlIdRole(String utlIdRole) {
		this.utlIdRole = utlIdRole;
	}
	/**
	 * @return the demande
	 */
	public List<DemandeBean> getDemande() {
		return demande;
	}
	/**
	 * @param demande the demande to set
	 */
	public void setDemande(List<DemandeBean> demande) {
		this.demande = demande;
	}
	/**
	 * @return the etablissementBean
	 */
	public EtablissementBean getEtablissementBean() {
		return etablissementBean;
	}
	/**
	 * @param etablissementBean the etablissementBean to set
	 */
	public void setEtablissementBean(EtablissementBean etablissementBean) {
		this.etablissementBean = etablissementBean;
	}

}
