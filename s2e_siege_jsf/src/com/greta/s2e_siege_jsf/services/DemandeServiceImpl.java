package com.greta.s2e_siege_jsf.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.greta.s2e_siege_jsf.beans.DemandeBean;
import com.greta.s2e_siege_jsf.dao.DemandeDaoImpl;

public class DemandeServiceImpl implements DemandeService{
	
	private static final Logger LOGGER = Logger.getLogger(DemandeServiceImpl.class);
	private DemandeDaoImpl dmdDao;
	
	

	/**
	 * 
	 */
	public DemandeServiceImpl() {
		super();
		dmdDao = new DemandeDaoImpl();
	}

	@Override
	public List<DemandeBean> listDemande() {
		LOGGER.info("Service listDemande");
		List<DemandeBean> dmdList = dmdDao.listDmd();
		return dmdList;
	}

	@Override
	public int create(DemandeBean demande) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(DemandeBean demande) {
		int result = 0;
		LOGGER.info("Service update demande");
		result = dmdDao.update(demande);
		return result;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public DemandeBean getElementById(int id) {
		return null;
	}

}
