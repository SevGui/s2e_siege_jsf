package com.greta.s2e_siege_jsf.services;

import java.util.List;

import org.apache.log4j.Logger;

import com.greta.s2e_siege_jsf.beans.EtablissementBean;
import com.greta.s2e_siege_jsf.dao.EtablissementDao;
import com.greta.s2e_siege_jsf.dao.EtablissementDaoImpl;

public class EtablissementServiceImpl implements EtablissementService{
	
	private static final Logger LOGGER = Logger.getLogger(EtablissementServiceImpl.class);
	private EtablissementDao etabDao;
	
	
	
	/**
	 * @param etabDao
	 */
	public EtablissementServiceImpl() {
		super();
		etabDao = new EtablissementDaoImpl();
	}

	@Override
	public List<EtablissementBean> listEtab() {
		LOGGER.info("Service listEtab");
		List<EtablissementBean> etabList = etabDao.listEtab();
		return etabList;
	}

	@Override
	public int create(EtablissementBean etab) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public int update(EtablissementBean etab) {
		int result = 0;
		LOGGER.info("Service update user");
		result = etabDao.update(etab);
		return result;
	}

	@Override
	public int delete(int id) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public EtablissementBean getElementById(int id) {
		LOGGER.info("Service getElementById");
		EtablissementBean etab = etabDao.getElementById(id);
		return etab;
	}

}
