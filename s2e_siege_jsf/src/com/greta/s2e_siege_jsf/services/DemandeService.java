package com.greta.s2e_siege_jsf.services;

import java.util.List;

import com.greta.s2e_siege_jsf.beans.DemandeBean;

public interface DemandeService {

	public List<DemandeBean> listDemande();
	public int create(DemandeBean demande);
	public int update(DemandeBean demande);
	public int delete(int id);
	public DemandeBean getElementById(int id);
}
