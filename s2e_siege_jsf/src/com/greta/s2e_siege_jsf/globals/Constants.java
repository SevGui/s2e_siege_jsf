package com.greta.s2e_siege_jsf.globals;

public class Constants {
	
	/**
	 * ATTRIBUTES
	 */	
	//Texte boutons
	public static final String MENU_BUDGET	 			= "Suivi Budget";
	public static final String MENU_DEMANDE 			= "Validation Demandes";
	public static final String MENU_DECONNEXION 		= "Déconnexion";
	public static final String BT_MODIFIER	 			= "Modifier";
	public static final String BT_VALIDER	 			= "Valider";
	public static final String BT_REFUSER	 			= "Refuser";
	
	/* Paramètres du cookie qui contiendra l'id de l'utilisateur connecté */
	public static final String COOKIE_S2E 				= "s2e_user";
	public static final String COOKIE_S2E_PATH 			= "/";

	/* Paramètres du session qui contiendra l'utilisateur connecté */
	public static final String SESSION_S2E 				= "s2e_user";
	
	/* Droits */
	public static final String DROIT_ETABLISSEMENT		= "SG";
	
	/* envoi vers les XHTML */
	public static final String BUDGET_PAGE				= "listbudget";
	public static final String UPDATE_PAGE				= "updatebudget";
	public static final String DEMANDE_PAGE				= "listdemandes";
//	public static final String REDIRECTION_LOGIN 		= "http://10.120.112.120:8080/s2e_accueil_struts/";
	public static final String REDIRECTION_LOGIN 		= "http://localhost:8080/s2e_accueil_struts/";
	
	/* Constante pour les requêtes */
	public static final String REFU_ETAT				= "REFU";
	public static final String VALI_ETAT				= "COUR";

	/* Nom de la table demandes et des colonnes de la base de données */
	public static final String DEMANDE					= "demandes";
	public static final String DMD_ID					= "dmdId";
	public static final String DMD_QTE					= "dmdQuant";
	public static final String DMD_DELAI				= "dmdDelai";
	public static final String DMD_DATE					= "dmdDate";
	public static final String DMD_ID_USER				= "dmdIdUtil";
	public static final String DMD_ID_ART				= "dmdIdArt";
	public static final String DMD_ID_ETAT				= "dmdIdEtat";
	
	/* Nom de la table etats et des colonnes de la base de données */
	public static final String ETATS					= "etats";
	public static final String ETT_ID					= "etatId";
	public static final String ETT_LIB					= "etatLibelle";
	
	/* Nom de la table articles et des colonnes de la base de données */
	public static final String ARTICLES					= "articles";
	public static final String ART_ID					= "artId";
	public static final String ART_NOM					= "artNom";
	public static final String ART_MARQUE				= "artMarque";
	public static final String ART_MODELE				= "artModele";
	public static final String ART_EQUI					= "artEqui";
	public static final String ART_PRIXM				= "artPrixMoyen";
	public static final String ART_QTETA				= "artQuanTA";
	
	/* Nom de la table articles et des colonnes de la base de données */
	public static final String STOCK					= "stock";
	public static final String STK_ID					= "stockId";
	public static final String STK_QUANT				= "stockQuant";
	public static final String STK_QUANTMIN				= "stockQuantMin";
	public static final String STK_QUANTMAX				= "stockQuantMax";
	public static final String STK_ID_ART				= "stockIdArt";
	
	/* Nom de la table utilisateurs et des colonnes de la base de données */
	public static final String USERS					= "utilisateurs";
	public static final String USR_ID					= "utlId";
	public static final String USR_NOM					= "utlNom";
	public static final String USR_PRENOM				= "utlPrenom";
	public static final String USR_LOGIN				= "utlLogin";
	public static final String USR_PASSWORD				= "utlMdp";
	public static final String USR_ID_ROLE				= "utlIdRole";
	public static final String USR_ID_ETB				= "utlIdEtab";
	
	/* Nom de la table etablissement et des colonnes de la base de données */
	public static final String ETAB						= "etablissement";
	public static final String ETB_ID					= "etabId";
	public static final String ETB_NOM					= "etabNom";
	public static final String ETB_ADR					= "etabAdres";
	public static final String ETB_CP					= "etabCP";
	public static final String ETB_VILLE				= "etabVille";
	public static final String ETB_BUDG					= "etabBudget";
	public static final String ETB_BUDG_REST			= "etabBudRest";
	
	public Constants() {
		// TODO Auto-generated constructor stub
	}


}
